package cbir

import (
	"encoding/base64"
	"io/ioutil"
	"os"
	"testing"
)

func readImage(name string) (string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	file, err := os.Open(pwd + "/ahash_test/" + name)
	if err != nil {
		return "", err
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(data), nil
}

func TestAll(t *testing.T) {
	lenna64, err := readImage("lenna.png")
	if err != nil {
		t.Fatal(err)
	}
	lennaS64, err := readImage("lenna-sample.png")
	if err != nil {
		t.Fatal(err)
	}
	parrots64, err := readImage("parrots.png")
	if err != nil {
		t.Fatal(err)
	}

	err = Register(lenna64, "lenna")
	if err != nil {
		t.Fatal(err)
	}
	err = Register(parrots64, "parrots")
	if err != nil {
		t.Fatal(err)
	}

	res, err := Search(lennaS64)
	if err != nil {
		t.Fatal(err)
	}
  if res != "lenna" {
		t.Fatal(err)
	}
}
