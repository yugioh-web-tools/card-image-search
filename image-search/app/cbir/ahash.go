package cbir

import (
	"bytes"
	"encoding/base64"
	"errors"
	"image/png"
	"math"

	"github.com/corona10/goimagehash"
)

type Index struct {
	Hash *goimagehash.ImageHash
	Tag string
}

var IndexDB []Index

func Base64ToAverageHash(imageBase64 string) (*goimagehash.ImageHash, error) {
	unbased, err := base64.StdEncoding.DecodeString(imageBase64)
	if err != nil {
		return nil, err
	}

	r := bytes.NewReader(unbased)
	img, err := png.Decode(r)
	if err != nil {
		println("can not to image")
		return nil, err
	}

	return goimagehash.AverageHash(img)
}

func Reset() {
	IndexDB = []Index{}
}

func Register(imageBase64 string, tag string) error {
	hash, err := Base64ToAverageHash(imageBase64)
	if err != nil {
		return errors.New("can not calc ahash")
	}

	IndexDB = append(IndexDB, Index{Hash: hash, Tag: tag})
	return nil
}

func Search(imageBase64 string) (string, error) {
	hash, err := Base64ToAverageHash(imageBase64)
	if err != nil {
		return "", errors.New("can not calc ahash")
	}

	var minTag string
	minDistance := math.MaxInt64
	for _, index := range IndexDB {
		dist, err := hash.Distance(index.Hash)
		if err != nil {
			return "", errors.New("can not calc distance")
		}
		if minDistance > dist {
			minTag = index.Tag
			minDistance = dist
		}
	}

	return minTag, nil
}
