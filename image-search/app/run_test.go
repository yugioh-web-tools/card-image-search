package app

import (
	"app/cbir"
	"bytes"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
)

func readImage(name string) (string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	file, err := os.Open(pwd + "/run_test/" + name)
	if err != nil {
		return "", err
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(data), nil
}

func TestRegisterLackBody(t *testing.T) {
	router := SetupRouter()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/register", nil)
	if err != nil {
		t.Fatal("")
	}
	router.ServeHTTP(w, req)

	if w.Result().StatusCode == 200 {
		t.Fatal("")
	}
}

func TestRegister(t *testing.T) {
	route := SetupRouter()
	w := httptest.NewRecorder()

	b64img, err := readImage("lenna.png")
	if err != nil {
		t.Fatal("")
	}
	body := `{"img":"` + b64img + `","tag":"testRegister"}`
	req, err := http.NewRequest("POST", "/register", bytes.NewBuffer([]byte(body)))
	if err != nil {
		t.Fatal("")
	}
	route.ServeHTTP(w, req)

	if w.Result().StatusCode != 200 {
		t.Fatal("")
	}
}

func postRegister(engine *gin.Engine, name string) error {
	w := httptest.NewRecorder()
	b64img, err := readImage(name)
	if err != nil {
		return err
	}
	body := `{"img":"` + b64img + `","tag":"` + name + `"}`
	req, err := http.NewRequest("POST", "/register", bytes.NewBuffer([]byte(body)))
	if err != nil {
		return err
	}
	engine.ServeHTTP(w, req)
	return nil
}

func TestSearch(t *testing.T) {
	cbir.Reset()

	route := SetupRouter()
	w := httptest.NewRecorder()

	err := postRegister(route, "lenna.png")
	if err != nil {
		t.Fatal("")
	}
	err = postRegister(route, "parrots.png")
	if err != nil {
		t.Fatal("")
	}

	b64img, err := readImage("lenna-sample.png")
	if err != nil {
		t.Fatal("")
	}
	body := `{"query":"` + b64img + `"}`
	req, err := http.NewRequest("POST", "/search", bytes.NewBuffer([]byte(body)))
	if err != nil {
		t.Fatal("")
	}
	route.ServeHTTP(w, req)

	if w.Result().StatusCode != 200 {
		t.Fatal("")
	}
	respBody, err := ioutil.ReadAll(w.Result().Body)
	if err != nil {
		t.Fatal("")
	}
	if string(respBody) != `{"tag":"lenna.png"}` {
		t.Fatal(string(respBody))
	}
}
