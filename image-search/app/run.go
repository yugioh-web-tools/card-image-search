package app

import (
	"app/cbir"

	"github.com/gin-gonic/gin"
)

func main() {
	engine := SetupRouter()
	engine.Run("127.0.0.1")
}

func SetupRouter() *gin.Engine {
	engine := gin.Default()

	engine.POST("/search", search)
	engine.POST("/register", register)

	return engine
}

func search(c *gin.Context) {
	type SearchBody struct {
		Query string `json:"query" binding:"required"`
	}
	var json SearchBody
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(400, gin.H{})
	}

	res, err := cbir.Search(json.Query)
	if err != nil {
		c.JSON(400, gin.H{})
	}

	c.JSON(200, gin.H{"tag": res})
}

func register(c *gin.Context) {
	type RegisterBody struct {
		Image string `json:"img" binding:"required"`
		Tag string `json:"tag" binding:"required"`
	}
	var json RegisterBody
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(400, gin.H{})
	}

	cbir.Register(json.Image , json.Tag)
	c.JSON(200, gin.H{})
}
